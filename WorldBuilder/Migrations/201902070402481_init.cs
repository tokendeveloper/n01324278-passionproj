namespace WorldBuilder.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Creators",
                c => new
                    {
                        CreatorId = c.Int(nullable: false, identity: true),
                        CreatorFName = c.String(nullable: false, maxLength: 255),
                        CreatorLName = c.String(nullable: false, maxLength: 255),
                        CreatorDispName = c.String(nullable: false, maxLength: 255),
                        CreatorBio = c.String(nullable: false, maxLength: 500),
                        CreatorOriginCountry = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.CreatorId);
            
            CreateTable(
                "dbo.Worlds",
                c => new
                    {
                        WorldId = c.Int(nullable: false, identity: true),
                        WorldName = c.String(nullable: false, maxLength: 255),
                        WorldOverview = c.String(nullable: false),
                        Creator_CreatorId = c.Int(),
                    })
                .PrimaryKey(t => t.WorldId)
                .ForeignKey("dbo.Creators", t => t.Creator_CreatorId)
                .Index(t => t.Creator_CreatorId);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ImageId = c.Int(nullable: false, identity: true),
                        ImagePath = c.String(nullable: false, maxLength: 255),
                        World_WorldId = c.Int(),
                    })
                .PrimaryKey(t => t.ImageId)
                .ForeignKey("dbo.Worlds", t => t.World_WorldId)
                .Index(t => t.World_WorldId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Images", "World_WorldId", "dbo.Worlds");
            DropForeignKey("dbo.Worlds", "Creator_CreatorId", "dbo.Creators");
            DropIndex("dbo.Images", new[] { "World_WorldId" });
            DropIndex("dbo.Worlds", new[] { "Creator_CreatorId" });
            DropTable("dbo.Images");
            DropTable("dbo.Worlds");
            DropTable("dbo.Creators");
        }
    }
}
