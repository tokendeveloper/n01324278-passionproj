﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorldBuilder.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net;

namespace WorldBuilder.Controllers
{
    public class CreatorController : Controller
    {
        private WorldBuilderContext db = new WorldBuilderContext();

        // GET: Creator
        public ActionResult List()
        {            
            //Pass db object to view.
            //Referenced Christine's code.
            return View(db.Creators);
        }

        public ActionResult CreatorProfile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Creator creator = db.Creators.Find(id);
            if (creator == null)
            {
                return HttpNotFound();
            }
            return View(creator);
        }

        //If there is no POST, just return the view for Add.
        public ActionResult Add()
        {
            return View();
        }

        //After POST submission, the following code will run. The page is then redirected to List View
        //Reference Christine's Code.
        [HttpPost]
        //http://www.binaryintellect.net/articles/20e546b4-3ae9-416b-878e-5b12434fe7a6.aspx
        [ValidateAntiForgeryToken]
        public ActionResult Add(string CreatorFName, string CreatorLName, string CreatorDispName, string CreatorOriginCountry, string CreatorBio, Creator creator)
        {
            //If the input matches the model's constraint, execute the following query.
            //Else, show view, with error message.

            if (ModelState.IsValid)
            {
                string insertQuery = "insert into Creators (CreatorFName, CreatorLName, CreatorDispName, CreatorOriginCountry, CreatorBio) values (@CreatorFName, @CreatorLName, @CreatorDispName, @CreatorOriginCountry, @CreatorBio)";

                SqlParameter[] queryParams = new SqlParameter[5];
                queryParams[0] = new SqlParameter("@CreatorFName", CreatorFName);
                queryParams[1] = new SqlParameter("@CreatorLName", CreatorLName);
                queryParams[2] = new SqlParameter("@CreatorDispName", CreatorDispName);
                queryParams[3] = new SqlParameter("@CreatorOriginCountry", CreatorOriginCountry);
                queryParams[4] = new SqlParameter("@CreatorBio", CreatorBio);

                db.Database.ExecuteSqlCommand(insertQuery, queryParams);
                Debug.WriteLine(insertQuery);
                return RedirectToAction("List");
            }
            return View(creator);
        }

        public ActionResult EditCreator(int? id)
        {
            Creator creator = db.Creators.Find(id);

            return View(creator);
        }

        //Edit
        //Referenced Christine's code
        [HttpPost]
        public ActionResult EditCreator(int? CreatorId, string CreatorFName, string CreatorLName, string CreatorDispName, string CreatorOriginCountry, string CreatorBio)
        {
            string updateQuery = "update Creators set CreatorFName = @CreatorFName, CreatorLName = @CreatorLName, CreatorDispName = @CreatorDispName, CreatorOriginCountry = @CreatorOriginCountry, CreatorBio = @CreatorBio " +
                 "where CreatorId = @CreatorId";

            SqlParameter[] queryParams = new SqlParameter[6];
            queryParams[0] = new SqlParameter("@CreatorFName", CreatorFName);
            queryParams[1] = new SqlParameter("@CreatorLName", CreatorLName);
            queryParams[2] = new SqlParameter("@CreatorDispName", CreatorDispName);
            queryParams[3] = new SqlParameter("@CreatorOriginCountry", CreatorOriginCountry);
            queryParams[4] = new SqlParameter("@CreatorBio", CreatorBio);
            queryParams[5] = new SqlParameter("@CreatorId", CreatorId);

            db.Database.ExecuteSqlCommand(updateQuery, queryParams);
            Debug.WriteLine(updateQuery);
            return RedirectToAction("List");
        }

        //Delete - Referenced Christine's code.
        public ActionResult DeleteCreator(int id)
        {
            //Access ICollections WorldId.
            //Loop thorugh all world in Worlds by this creator.
            //Delete from image table where World_WorldId = WorldId.
            //This won't actually delete the image from the file...need to still handle that.
            var worldList = db.Creators.Find(id).Worlds;

            foreach (var world in worldList)
            {
                var worldId = world.WorldId;

                string queryString = "DELETE FROM images WHERE World_WorldID=@wid";
                SqlParameter param = new SqlParameter("@wid", worldId);

                db.Database.ExecuteSqlCommand(queryString, param);
            }

            string delWorldQuery = "delete from worlds where creator_creatorid = @id";
            db.Database.ExecuteSqlCommand(delWorldQuery, new SqlParameter("@id", id));


            string delCreatorQuery = "delete from creators where creatorid = @id";
            db.Database.ExecuteSqlCommand(delCreatorQuery, new SqlParameter("@id", id));

            //This method was finnicky as heck so I'll stick to using query strings.....
            //https://stackoverflow.com/questions/43257512/deleting-records-from-nested-tables-mvc-5
            //Delete from child table (worlds) before removing parent table (creators).
            //Creator creator = db.Creators.FirstOrDefault(c => c.CreatorId == id);

            //foreach (var item in creator.Worlds.ToList())
            //{
            //    foreach (var creators in item.Creator.Worlds.ToList())
            //    {
            //        db.Worlds.Remove(creators);
            //    }
            //    db.Worlds.Remove(item);
            //}

            //db.Creators.Remove(creator);
            db.SaveChanges();

            return RedirectToAction("List");
        }

        public ActionResult DeleteWorld(int id)
        {
            //Delete world 
            string delImgQuery = "delete from images where world_worldId = @id";
            db.Database.ExecuteSqlCommand(delImgQuery, new SqlParameter("@id", id));


            string delWorldQuery = "delete from worlds where worldId = @id";
            db.Database.ExecuteSqlCommand(delWorldQuery, new SqlParameter("@id", id));


            //https://stackoverflow.com/questions/43257512/deleting-records-from-nested-tables-mvc-5
            //Delete from child table (images) before removing parent table (world).
            //World world = db.Worlds.FirstOrDefault(w => w.WorldId == id);

            //foreach (var item in world.Images.ToList())
            //{   
            //    foreach (var worlds in item.World.Images.ToList())
            //    {
            //        db.Images.Remove(worlds);
            //    }
            //    db.Images.Remove(item);
            //db.Worlds.Remove(world);
            db.SaveChanges();
            //Redirect to previous link (ie creator profile).
            //https://stackoverflow.com/questions/815229/how-do-i-redirect-to-the-previous-action-in-asp-net-mvc
            return Redirect(Request.UrlReferrer.ToString());
        }
    }
}