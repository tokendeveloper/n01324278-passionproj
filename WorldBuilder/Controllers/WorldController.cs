﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorldBuilder.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net;

namespace WorldBuilder.Controllers
{
    public class WorldController : Controller
    {
        private WorldBuilderContext db = new WorldBuilderContext();

        // GET: World
        public ActionResult WorldList()
        {
            return View();
        }

        public ActionResult WorldDetails(int? id)
        {
            World world = db.Worlds.Find(id);

            return View(world);
        }


        public ActionResult AddWorld()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddWorld(string WorldName, string WorldOverview, string CreatorId, World world)
        {
            string insertQuery = "insert into Worlds (WorldName, WorldOverview, creator_CreatorId) values (@WorldName, @WorldOverview, @CreatorId)";
            //get id from url
            //https://stackoverflow.com/questions/24673292/get-id-from-url-in-an-action-from-a-controller-with-asp-net?rq=1
            //var creatorId = Url.RequestContext.RouteData.Values["id"];

            if (ModelState.IsValid)
            {
                SqlParameter[] queryParams = new SqlParameter[3];
                queryParams[0] = new SqlParameter("@WorldName", WorldName);
                queryParams[1] = new SqlParameter("@WorldOverview", WorldOverview);
                queryParams[2] = new SqlParameter("@CreatorId", CreatorId);

                db.Database.ExecuteSqlCommand(insertQuery, queryParams);
                Debug.WriteLine(insertQuery);
                return RedirectToAction("CreatorProfile/" + CreatorId, "Creator");
            }

            return View(world);
        }

        public ActionResult EditWorld(int id)
        {
            World world = db.Worlds.Find(id);

            return View(world);
        }

        [HttpPost]
        //Referenced from Christine's code! Thanks :D 
        public ActionResult EditWorld(int id, string WorldName, string WorldOverview, World world)
        {
            string updateQuery = "update Worlds set WorldName = @WorldName, WorldOverview = @WorldOverview where WorldId = @id";

            world = db.Worlds.Find(id);

            SqlParameter[] queryParams = new SqlParameter[3];
            queryParams[0] = new SqlParameter("@WorldName", WorldName);
            queryParams[1] = new SqlParameter("@WorldOverview", WorldOverview);
            queryParams[2] = new SqlParameter("@id", id);


            db.Database.ExecuteSqlCommand(updateQuery, queryParams);
            Debug.WriteLine(updateQuery);
            return RedirectToAction("WorldDetails/" + id);

        }

        public ActionResult AddImage()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //handle file posting
        //https://stackoverflow.com/questions/16255882/uploading-displaying-images-in-mvc-4
        public ActionResult AddImage(Image image, int WorldId, HttpPostedFileBase file)
        {
            string insertQuery = "insert into Images (ImagePath, World_WorldId) values (@ImagePath, @WorldId)";

            if (ModelState.IsValid)
            {
                //If file is not empty, run the query.
                if (file != null)
                {
                    //Need to validate file type for images.

                    //Modify the filename to current date (year, month, day, minute, seconds) + file extension to create unique image file name.
                    //https://stackoverflow.com/questions/24625078/how-generate-a-unique-file-name-at-upload-files-in-webserver-mvc
                    var fileName = DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);

                    //Save the file in the root/Images/, and as its FileName.
                    file.SaveAs(HttpContext.Server.MapPath("~/Images/") + fileName);

                    image.ImagePath = fileName;

                    SqlParameter[] queryParams = new SqlParameter[2];
                    queryParams[0] = new SqlParameter("@ImagePath", image.ImagePath);
                    queryParams[1] = new SqlParameter("@WorldId", WorldId);
                    db.Database.ExecuteSqlCommand(insertQuery, queryParams);
                    Debug.WriteLine(insertQuery);
                }
                db.SaveChanges();
                return RedirectToAction("WorldDetails/" + WorldId);
            }

            return RedirectToAction("AddImage/" + WorldId);
        }

    }
}