﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorldBuilder.Models
{
    public class Image
    {
        //images model
        //https://stackoverflow.com/questions/16255882/uploading-displaying-images-in-mvc-4
        [Key]
        public int ImageId { get; set; }

        [StringLength(255), Display(Name = "Image Path")]
        public string ImagePath { get; set; }

        //World Foreign Key
        //Taken from Christine's code
        public virtual World World { get; set; }
    }
}