﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorldBuilder.Models
{
    public class World
    {
        [Key]
        public int WorldId { get; set; }


        //Add custom error message
        //https://stackoverflow.com/questions/6587816/how-to-change-the-errormessage-for-int-model-validation-in-asp-net-
        [Required(ErrorMessage = "Please enter a name for your world."), StringLength(255), Display(Name = "World Name")]
        public string WorldName { get; set; }

        [Required(ErrorMessage = "Please enter a name for your world."), StringLength(int.MaxValue), Display(Name = "World Overview")]
        public string WorldOverview { get; set; }

        //Page Author
        public virtual Creator Creator { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}