﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorldBuilder.Models
{
    public class Creator
    {
        [Key]
        public int CreatorId { get; set; }
        //Add custom error message
        //https://stackoverflow.com/questions/6587816/how-to-change-the-errormessage-for-int-model-validation-in-asp-net-mvc
        [Required(ErrorMessage = "Please enter your first name."), StringLength(255), Display(Name = "First Name")]
        public string CreatorFName { get; set; }

        [Required(ErrorMessage = "Please enter your last name."), StringLength(255), Display(Name = "Last Name")]
        public string CreatorLName { get; set; }

        [Required(ErrorMessage = "Please enter your preferred display name."), StringLength(255), Display(Name = "Display Name")]
        public string CreatorDispName { get; set; }


        [Required(ErrorMessage = "Please enter a short bio about yourself."), StringLength(500), Display(Name = "Bio")]
        public string CreatorBio { get; set; }

        [StringLength(255), Display(Name = "Country of Origin")]
        public string CreatorOriginCountry { get; set; }

        public virtual ICollection<World> Worlds { get; set; }

    }
}